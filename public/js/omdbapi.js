function tampilData(){

    $.ajax({
        async: true,
        cache: false,
            url : 'http://omdbapi.com',
            typeData : 'json',
            type : 'get',
            data : {
                'apikey': '69c545a3',
                's':'avengers',
            },
            success: function(result){
                if(result.Response === 'True'){
                    let datas = result.Search;
                    $.each(datas, function(index, data){
                     
                        $('#list_data').append(`
                        <div class="col-3 mb-4">
                            <div class="card">
                                <img src="${data.Poster}"  height="400" class="card-img-top" alt="...">
                                <div class="card-body">
                                    <h6 class="card-title">${data.Title} </h6>
                                    <p class="card-text">${data.Year}</p>
                                    <a href="#" class="card-link detail_data" data-toggle="modal" data-target="#exampleModal" data-id="${data.imdbID}">Preview</a>
                                </div>
                            </div>
                        </div>
                        `);
                    })
                }else{
                    $('#list_data').html(`
                    <h1> 
                        Data ${result.Error}
                    </h1>
                    `);
                }
            }
        })
}
function detailData(id){
    $.ajax({
        url : 'http://omdbapi.com',
        typeData : 'json',
        type : 'get',
        data : {
            'apikey': '69c545a3',
            'i':id,
        },
        success: function(data){
            if(data.Response === 'True'){
                $('#title_data').html(`
                <h5 class="modal-title text-center" id="title_data">${data.Title}</h5>
                `);
                $('#body_data').append(`
                <div class="card mb-3" style="max-width: auto;">
                <div class="row no-gutters">
                  <div class="col-md-4">
                    <img src="${data.Poster}" class="card-img" alt="...">
                  </div>
                  <div class="col-md-8">
                    <div class="card-body">
                      <h5 class="card-title">Title      : ${data.Title}</h5>
                      <p class="card-text"> Director    : ${data.Director}</p>
                      <p class="card-text"> Genre       : ${data.Genre}</p>
                      <p class="card-text"> Language    : ${data.Title}</p>
                      <p class="card-text"> Actors      : ${data.Actors}</p>
                      <p class="card-text"> Released      : ${data.Released}</p>
                    </div>
                  </div>
                </div>
              </div>
                `);
            }else{

            }
        }
    })
}
function getData(){
    $.ajax({
        url: 'http://omdbapi.com/',
        type:'get',
        dataType : 'json',
        data:{
            'apikey' : '69c545a3',
            's':$('#input_data').val(),
        },
        success: function(result){
            $('#list_data').html('');
            if(result.Response === 'True'){
                let datas = result.Search;
                $.each(datas, function(index, data){
                    $('#list_data').append(`
                    <div class="col-3 mb-4">
                        <div class="card">
                            <img src="${data.Poster}"  height="400" class="card-img-top" alt="...">
                            <div class="card-body">
                                <h6 class="card-title">${data.Title} </h6>
                                <p class="card-text">${data.Year}</p>
                                <a href="#" class="card-link detail_data" data-toggle="modal" data-target="#exampleModal" data-id="${data.imdbID}">Preview</a>
                            </div>
                        </div>
                    </div>
                    `);
                })
            }else{
                $('#list_data').html(`
                <h1> 
                    Data ${result.Error}
                </h1>
                `);
            }
        }
        
    });
}
tampilData();

$('#input_data').on('keyup',function(e){
    if(e.keyCode === 13){
        getData();
    }
})
$('#search_data').on('click',function(){
    getData();
});
$('#list_data').on('click','.detail_data',function(){
    let id = $(this).data('id');
    detailData(id);
})
