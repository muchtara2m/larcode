<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Approvals extends Model
{
    //
    protected $table = 'approvals';
    protected $fillable = ['username','name','min','queue','max'];

   
}
