<?php

namespace App\Http\Controllers\Aybis;

use App\Http\Controllers\Controller;
use App\Http\Requests\ApprovalsRequest;
use App\Approvals;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class ApprovalsController extends Controller {

    public function createApproval(){
        return view('aybis.approvals.create');
    }

    public function index(){
        return view('aybis.approvals.index');
    }

    public function storeApproval(ApprovalsRequest $request){
        
        DB::beginTransaction();
        $approval = Approvals::create($request->all());
        try {
            DB::commit();
            return redirect()->back();
        }catch(\Exception $e){
            DB::rollback();
            return redirect()->back()
                ->withErrors("Something wrong from the server, please check carefully")
                ->withInput();
        }

    }
}