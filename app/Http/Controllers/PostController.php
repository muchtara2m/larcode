<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;


class PostController extends Controller
{
    public function create(){
        return view('posts.create');
    }

    public function store(Request $request){
        $post = new Post;
        $post->title = $request->title;
        $post->content = $request->content;
        $post->is_published = $request->is_published;
        $post->save();

        return redirect()->route('index-post')->with('success', 'Berhasil Menambah Data');
    }

    public function index(){
        // $posts = Post::where('is_published', true)->get();
        $posts = Post::latest()->get();
        return view('posts.index', compact('posts'));
    }
}
