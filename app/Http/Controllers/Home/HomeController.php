<?php

namespace App\Http\Controllers\Home;


use App\Http\Controllers\Controller;
use App\Http\Requests\Home\DataDiriFormRequest;
use App\Mail\Home\DataDiriVerificationEmail;
use App\Mail\UserVerificationEmail;
use Symfony\Component\HttpFoundation\Request;
use Illuminate\Support\Facades\Mail;
use Faker\Factory as Faker;
use App\User;
use App\Approvals;



class HomeController extends Controller
{
    public function index(Request $request){
        $user = User::agegreaterthan(20)
                    ->orderBy('name',$request->get('order','asc'))
                    ->paginate($request->get('per-page',10));
       return view('home.home',compact('user'));
    }

    public function fakerDataUser(Request $request){
        $faker = Faker::create('id_ID');
        for($i = 1; $i <= 200; $i++){
            // insert data ke table pegawai menggunakan Faker
            User::insert([
                'username' =>  $faker->username,
                'name' =>  $faker->name,
                'email' => preg_replace('/@example\..*/', '@gmail.com', $faker->unique()->safeEmail),
                'password' => bcrypt('secret'),
                'no_telp' => $faker->phoneNumber,
                'active' => $faker->boolean($chanceOfGettingTrue = 90),
                'umur' => $faker->numberBetween($min = 10, $max = 100),
             ]);
        }        
    }

    public function show(User $user){
        return($user);
    }

    public function dataDiri(){
        return new UserVerificationEmail();

        // return view('home.data_diri');
    }

    public function testingPost(Request $request){
        $this->validate($request, [
            'nama' => 'required',
            'email' => 'required|email',
        ],[
            'required' => ':Attribute harus diisi',
            'email' => ':Attribute tidak sesuai',
        ]);

        dd($request->nama, $request->email);
    }

    public function storeDataDiri(DataDiriFormRequest $request){

        // menggunakan form class request pada folder \request
        // dd('Data diri berhasil dimasukkan');
    //    Mail::to("$request->nama@mail.com")->send(new DataDiriVerificationEmail());
        // use email markdown
       Mail::to($request->email)->send(new UserVerificationEmail());
       if (Mail::failures()) {
        // return response showing failed emails
        }else{
            dd('bisa markdown');
        }

        return redirect()->route('dataDiri')->with('success',"Data $request->nama berhasil dimasukkan");
    }

}
