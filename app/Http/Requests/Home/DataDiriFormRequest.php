<?php

namespace App\Http\Requests\Home;

use Illuminate\Foundation\Http\FormRequest;

class DataDiriFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // tidak perlu login untuk input data
        return true;
        // butuh login untuk input
        // return auth()->check();

    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama' => 'required',
            'email' => 'required|email',
            'telp'=> 'required|min:11'
        ];
    }
}
