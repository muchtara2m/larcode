<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ApprovalsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // tidak perlu login untuk input data
        return true;
        // butuh login untuk input
        // return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
                'name' => 'required',
                'min' => 'required|min:1',
                'max' => 'required|max:20',
                'queue' => 'required',
        ];
        $rules['username'];

        return $rules;
        $validator =  $this->validate([
            'username' => 'required|unique:approvals',
        ]);
        if($this->status == 'save'){

            $validator =  $this->validate([
                'name' => 'required',
                'min' => 'required|min:1',
                'max' => 'required|max:20',
                'queue' => 'required',
            ]);
        }
        return $validator;
    }
}
