<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','umur','active','no_telp','username'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // select data where user is active
    public function scopeIsActive($query){
        return $query->where('active', false);
    }

    public function scopeIsNotActive($query){
        return $query->where('active',true);
    }

    public function scopeAgeGreaterThan($query, $value){
        return $query->where('umur','>',$value);
    }

    public function getRouteKeyName(){
        return 'username';
    }
}
