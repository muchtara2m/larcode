@component('mail::message')
# Welcome in {{ config('app.name') }}

This activation code for your account, please click link below

@component('mail::button', ['url' => ''])
SZR666
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
