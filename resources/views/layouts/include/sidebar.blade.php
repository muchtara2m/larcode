<div class="be-left-sidebar">
    <div class="left-sidebar-wrapper"><a href="#" class="left-sidebar-toggle">Dashboard</a>
        <div class="left-sidebar-spacer">
            <div class="left-sidebar-scroll">
                <div class="left-sidebar-content">
                    <ul class="sidebar-elements">
                        <li class="divider">Menu</li>
                        <li class="active"><a href="index.html"><i
                                    class="icon mdi mdi-home"></i><span>Dashboard</span></a>
                        </li>
                        <li class="parent"><a href="#"><i class="icon mdi mdi-face"></i><span>UI Elements</span></a>
                            <ul class="sub-menu">
                                <li><a href="ui-alerts.html">Alerts</a>
                                </li>
                                <li><a href="ui-buttons.html">Buttons</a>
                                </li>
                                <li><a href="ui-panels.html">Panels</a>
                                </li>
                                <li><a href="ui-general.html">General</a>
                                </li>
                                <li><a href="ui-modals.html">Modals</a>
                                </li>
                                <li><a href="ui-notifications.html">Notifications</a>
                                </li>
                                <li><a href="ui-icons.html">Icons</a>
                                </li>
                                <li><a href="ui-grid.html">Grid</a>
                                </li>
                                <li><a href="ui-tabs-accordions.html">Tabs &amp; Accordions</a>
                                </li>
                                <li><a href="ui-nestable-lists.html">Nestable Lists</a>
                                </li>
                                <li><a href="ui-typography.html"><span
                                            class="label label-primary pull-right">New</span>Typography</a>
                                </li>
                            </ul>
                        </li>
                        <li class="parent"><a href="charts.html"><i
                                    class="icon mdi mdi-chart-donut"></i><span>Charts</span></a>
                            <ul class="sub-menu">
                                <li><a href="charts-flot.html">Flot</a>
                                </li>
                                <li><a href="charts-sparkline.html">Sparklines</a>
                                </li>
                                <li><a href="charts-chartjs.html">Chart.js</a>
                                </li>
                                <li><a href="charts-morris.html">Morris.js</a>
                                </li>
                            </ul>
                        </li>
                        <li class="parent"><a href="#"><i class="icon mdi mdi-dot-circle"></i><span>Forms</span></a>
                            <ul class="sub-menu">
                                <li><a href="form-elements.html">Elements</a>
                                </li>
                                <li><a href="form-validation.html">Validation</a>
                                </li>
                                <li><a href="form-multiselect.html"><span
                                            class="label label-primary pull-right">New</span>Multiselect</a>
                                </li>
                                <li><a href="form-wizard.html">Wizard</a>
                                </li>
                                <li><a href="form-masks.html">Input Masks</a>
                                </li>
                                <li><a href="form-wysiwyg.html">WYSIWYG Editor</a>
                                </li>
                                <li><a href="form-upload.html">Multi Upload</a>
                                </li>
                                <li><a href="form-editable.html"><span
                                            class="label label-primary pull-right">New</span>X-editable</a>
                                </li>
                            </ul>
                        </li>
                        <li class="parent"><a href="#"><i class="icon mdi mdi-border-all"></i><span>Tables</span></a>
                            <ul class="sub-menu">
                                <li><a href="tables-general.html">General</a>
                                </li>
                                <li><a href="tables-datatables.html">Data Tables</a>
                                </li>
                            </ul>
                        </li>

                        <li class="divider">Master Data</li>
                        <li class="parent"><a href="#"><i class="icon mdi mdi-inbox"></i><span>Email</span></a>
                            <ul class="sub-menu">
                                <li><a href="email-inbox.html">Inbox</a>
                                </li>
                                <li><a href="email-read.html">Email Detail</a>
                                </li>
                                <li><a href="email-compose.html">Email Compose</a>
                                </li>
                            </ul>
                        </li>
                        <li class="parent"><a href="#"><i class="icon mdi mdi-layers"></i><span>Master Data</span></a>
                            <ul class="sub-menu">
                                <li><a href="pages-blank.html">Data User</a>
                                </li>
                                <li><a href="pages-blank-header.html">Data Roles</a>
                                </li>
                                <li><a href="pages-login.html">Data Menu</a>
                                </li>
                                <li><a href="pages-login2.html">Data Makanan</a>
                                </li>
                                <li><a href="pages-404.html">Data Minuman</a>
                                </li>
                                <li><a href="pages-sign-up.html">Data Kue</a>
                                </li>
                                <li><a href="pages-forgot-password.html">Data </a>
                                </li>
                                <li><a href="pages-timeline.html"><span
                                            class="label label-primary pull-right">New</span>Timeline</a>
                                </li>
                                <li><a href="pages-timeline2.html"><span
                                            class="label label-primary pull-right">New</span>Timeline v2</a>
                                </li>
                                <li><a href="pages-invoice.html"><span
                                            class="label label-primary pull-right">New</span>Invoice</a>
                                </li>
                                <li><a href="pages-calendar.html">Calendar</a>
                                </li>
                                <li><a href="pages-gallery.html">Gallery</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="progress-widget">
            <div class="progress-data"><span class="progress-value">60%</span><span class="name">Current Project</span>
            </div>
            <div class="progress">
                <div style="width: 60%;" class="progress-bar progress-bar-primary"></div>
            </div>
        </div>
    </div>
</div>
