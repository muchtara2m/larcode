@extends('layouts.master')

@php
$homelink = "/home";
$crpagename = "Approval";
@endphp

@section('title')
{{ $crpagename." | Demo" }}
@endsection

@section('stylesheets')


@endsection
@section('customstyle')
<style type="text/css">
</style>
@endsection

@section('content')
<div class="be-content">
    <div class="page-head">
        <h2 class="page-head-title">Create Approvals</h2>
        <ol class="breadcrumb page-head-nav">
            <li><a href="#">Home</a></li>
            <li><a href="#">Approval</a></li>
            <li class="active">Create</li>
        </ol>
    </div>
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default panel-border-color panel-border-color-primary">
                    <div class="panel-heading panel-heading-divider">Approval Form
                        <span class="panel-subtitle">This is the default bootstrap form layout</span></div>
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="/store-approval">
                            @csrf
                            <div class="form-group xs-mt-10">
                                <label class="col-sm-2 control-label">Nama</label>
                                <div class="col-sm-10">
                                <input id="name" type="text" name="name" placeholder="Nama" class="form-control" value="{{ old('name')}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Username</label>
                                <div class="col-sm-10">
                                    <input id="username" type="text" name="username" placeholder="Username"
                                        class="form-control" value="{{ old('username')}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Queue</label>
                                <div class="col-sm-10">
                                    <input id="queue" type="text" name="queue" placeholder="Queue" class="form-control" value="{{ old('queue')}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Min</label>
                                <div class="col-sm-10">
                                    <input id="min" type="text" name="min" placeholder="Min" class="form-control" value="{{ old('min')}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Max</label>
                                <div class="col-sm-10">
                                    <input id="max" type="text" name="max" placeholder="Max" class="form-control" value="{{ old('max')}}">
                                </div>
                            </div>
                            <div class="row pt-3 mt-1">
                                <div class="col-sm-6">
                                    <button class="btn btn-primary" value="save" name="status">Submit</button>
                                    <button class="btn btn-success" value="draft" name="status">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

@section('scripts')
<script>
    $('').on('click', function () {
        var username = $('#username').val();
        var name = $('#name').val();
        var queue = $('#queue').val();
        var min = $('#min').val();
        var max = $('#max').val();

        $.ajax({
            url: '/store-approval',
            type: 'POST',
            // dataType: 'json',
            data: {
                "_token": "{{ csrf_token() }}",
                username: username,
                name: name,
                queue: queue,
                min: min.split('.').join(""),
                max: max.split('.').join(''),
            },
            success: function (data) {
                console.log('SUCCESS', data);
            },
            error: function (data) {
                console.log(data);
            }
        });
    });

</script>

@endsection
