<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Home</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>

<body>
    <div class="container">
        <h1 class="text-center">Testing Form</h1>
        <div class="row">
            <div class="col">
                <form action="{{ url('home-post') }}" method="post" class="form-group">
                    @csrf
                    <div class="col">
                        <div class="row form-group">
                            <div class="col-md-2">
                                    <label for="nama">Nama</label>
                            </div>
                            <div class="col-md-10">
                                    <input type="text" class="form-control {{ $errors->has('nama') ? 'is-invalid' : '' }}" placeholder="Masukkan Nama" name="nama" id="nama" value="{{ old('nama') }}">
                                    <div class="invalid-feedback">
                                        @if ($errors->has('nama'))
                                            {{ $errors->first() }}
                                        @endif
                                    </div>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="row form-group">
                            <div class="col-md-2">
                                    <label for="email">Email</label>
                            </div>
                            <div class="col-md-10">
                                    <input type="text" class="form-control {{ $errors->has('email') ? 'is-invalid':'' }}" placeholder="test@gmail.com" name="email" id="email" value="{{ old('email') }}">
                                    <div class="invalid-feedback">
                                            @if ($errors->has('email'))
                                            {{ $errors->first('email') }}
                                        @endif
                                    </div>
                            </div>
                        </div>
                    </div>
                    <div class="row form-group mx-md-5">
                            <button type="submit" class="btn btn-primary btn-lg btn-block" id="submit">Save</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="row mt-4">
            <div class="col">
                <div class="box">
                    <div class="box-body">
                        <div>
                                Per Page : 
                                <a href="?per-page=10">10</a>
                                <a href="?per-page=30">30</a>
                                <a href="?per-page=50">50</a>
                                <a href="?per-page=100">100</a>
                                <br>
                                Sorting :
                                <a href="?order=asc">Ascending</a>
                                <a href="?order=desc">Descending</a>
                        </div>
                      
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Name</th>
                                    <th>Username</th>
                                    <th>Email</th>
                                    <th>No Telp</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $no =1;
                                @endphp
                                @foreach ($user as $item)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $item->name }}</td>
                                    <td>{{ $item->username }}</td>
                                    <td>{{ $item->email }}</td>
                                    <td>{{ $item->no_telp }}</td>
                                    <td>{{ $item->active == 0 ? 'Belum Active' : 'Active' }}</td>
                                    <td>Edit</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="mx-auto" style="width: 200px;">
                            {{ $user->appends(request()->only('per-page','order'))->render() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
    </script>
</body>

</html>
