<?php

Route::get('/test-config', function () {
    // return view('welcome');
    return config('configTesting.pagination.admin');
});

Route::get('/data', function(){
    $date = date('Y-m-d');
    return "Sekarang tanggal $date";
});

// parameter dengan 2 nilai
Route::get('data-details/{idNya}/kendaraan/{kendaraan}',function($idNya, $kendaraan){
    if($idNya == 2){
        return "Data Detail User dengan Nama Abdul Muchtar memiliki kendaraan $kendaraan";
    }
    if($idNya == 1 ){
        return "Data Detail User dengan Nama Astria memiliki kendaraan $kendaraan";
    }
});

//parameter dengan nilai optional
Route::get('/data-detail/{id?}', function($id = null){
    if($id == null){
        return "ID tidak ditemukan $id";
    }
    if($id != null){
        return "Details ID dengan nomor $id";
    }
});
Route::get('/login', function () {
    return 'Anda Berhasil Login';
})->name('login');

Route::prefix('pay')->middleware('auth')->group(function () {

    Route::group(['prefix' => 'payment'], function () {
        Route::get('metode-pembayaran', function(){
            return "Metode Pembayaran";
        });

        Route::get('detail-pembayaran', function(){
            return "Detail Pembayaran";
        });
    });


});

Route::group(['prefix' => 'user'], function () {
    //definisi group route user
    Route::group(['prefix' => 'account'], function () {

        Route::group(['prefix' => 'setting'], function () {

            Route::get('profile', function () {
                return 'Profle User';
            });

            Route::get('photo', function(){
                return "Photo User";
            });

            Route::get('change-password', function () {
                return "Change Password User";
            });
        });
    });
});

Route::get('redirect', function () {
    return redirect()->route('homepage');
});

Route::get('homepage/landing-page', function () {
    return "Landing Page";
})->name('homepage');

Route::resource('article', 'ArticleController');

// for make data faker
Route::get('/seed-post', function (\App\Post $post) {
    $faker = Faker\Factory::create();

    foreach(range(1, 100) as $x){
        $post->create([
            'title' => $faker->sentence(5),
            'content' => $faker->sentence(50),
        ]);
    }
});
// for make data faker
// Route::get('/seed-user','Home\HomeController@fakerDataUser');
// end testing--------------------------------------------------------------
Route::get('/get-zomato','Home\HomeController@testingZomato');
Route::get('/','Home\HomeController@index');
Route::get('/create-approval','Aybis\ApprovalsController@createApproval');
Route::post('/store-approval','Aybis\ApprovalsController@storeApproval');
Route::get('/index-approval','Aybis\ApprovalsController@storeApproval');
Route::get('/master', function () {
    return view('layouts.master');
});

// Route::get('/detail-user/{user}','Home\HomeController@show');
// Route::get('/data-diri','Home\HomeController@dataDiri')->name('dataDiri');
// Route::post('/home-post','Home\HomeController@testingPost')->name('home-post');
// Route::post('/store-data-diri','Home\HomeController@storeDataDiri')->name('store-data-diri');

// route post -----------------------
// Route::get('/create-post', 'PostController@create');
// Route::post('/store-post','PostController@store')->name('store-post');
// Route::get('/index-post','PostController@index')->name('index-post');

// ----------------------------------------------------------------------------------------------------------
// service container

// end service container -------------------------------------------------------------------------------------------
Auth::routes(['verify' => true]);

Route::group(['middleware' => 'auth'], function(){
    Route::get('/home-user',function(){
        return view('aybis.home.index')->middleware('verified');
    });
});
// Route::get('/home', 'HomeController@index')->name('home')->middleware('verified');
