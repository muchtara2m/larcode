<?php
Auth::routes(['verify' => true]);
Route::group(['middleware' => 'auth'], function(){
    Route::get('/home-user',function(){
        return view('aybis.home.index');
    });
});
Route::get('/home', 'HomeController@index')->name('home')->middleware('verified');
Route::get('/master', function () {
    return view('layouts.master');
});
